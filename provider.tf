terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 4.18.0"
        }
    }

    backend "http" {
        address="https://gitlab.com/api/v4/projects/53811559/terraform/state/$TF_STATE_NAME"
        lock_address="https://gitlab.com/api/v4/projects/53811559/terraform/state/$TF_STATE_NAME/lock"
        unlock_address="https://gitlab.com/api/v4/projects/53811559/terraform/state/$TF_STATE_NAME/lock"
    }   
}   

provider "aws" {
    region = "us-east-1"

    access_key = "<AWS_ACCESS_KEY_ID>"
    secret_key = "<AWS_SECRET_KEY_ID>"
}
